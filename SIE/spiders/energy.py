# -*- coding: utf-8 -*-
import scrapy
"""
Use splash to handle javascript from the SIE website. 
The splash IP address must be specified in settings.py for production
"""
from scrapy_splash import SplashFormRequest

class EnergySpider(scrapy.Spider):
	name = 'energy'
	"""Query used when params are sent with the request and retrieve all data."""
	url_to_apply_options = 'http://sie.energia.gob.mx/bdiController.do?action=cuadro&subAction=applyOptions'
	start_urls = ['http://sie.energia.gob.mx/bdiController.do?action=cuadro&cvecua=IIIA1C01']

	"""Method used to send the main request using splash to send formdata"""
	def parse(self, response):
		data_request = {
			'datosde': 'REALES',
			'periodicidad': '1',
			'mesini': '01',
			'anoini': '2002',
			'mesfin': '07',
			'anofin': '2017',
			'datosdeSelect2': 'REALES',
			'anocompararSelect': '2018',
			'unidador': 'MWh',
			'unidadde': 'GWh',
			'variaRespectoRadio': 'mismoperiodo',
			'varPeriodoFijoSelect': '01',
			'varAnoFijoSelect': '2018',
			'columnaComparaRadio': 'variacion',
			'tipoVariacionRadio': 'RELATIVA',
			'lineaParametros': 'MENSUAL,01/2002-07/2018,REALES',
			'lineaParametrosLabel': 'MENSUAL,01/2002-07/2018,REALES',
			'lineaUnidades': '',
			'nParam': '0',
			'comparaFlag': 'null',
			'variaFlag': 'null',
			'columnaComparaFlag': 'null',
			'variaRespectoFlag': 'null',
			'combinaFlag': 'null',
			'tipoVariaFlag': 'null',
			'despTotFlag': 'null',
			'despAcumFlag': 'null',
			'tipoParam': '1',
			'avanzadas': 'false'
		}
		yield SplashFormRequest(url=self.url_to_apply_options, formdata=data_request, callback=self.parse_records)
	
	"""
	Methods to parse data in the format required
	:param response: Response from sie request with information needed
	"""
	def parse_records(self, response):
		data_sener = response.xpath('//*[@id="cuadroTable"]//tbody//tr')

		"""Extract row date information and slice array from third argument to the last one."""
		dates = data_sener[1].xpath('td//text()').extract()[3::]
		
		"""For loop used to collect all from the states. Ignoring ascii characters needed to avoid error when creating the csv"""
		for row in data_sener[4:31]:
			columns = row.xpath('td//text()')

			"""For loop used to iterate through data to get the amount and date information from the dates array"""
			for idx, data in enumerate(columns[2::]):
				date = dates[idx]
				record = {
					'state_name' : row.xpath('td//text()')[0].extract().encode('ascii', 'ignore').decode('ascii'),
					'amount': data.extract().encode('ascii', 'ignore').decode('ascii'),
					'month': date[:3],
					'year': date[4:]
				}
				yield record
