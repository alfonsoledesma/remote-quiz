FROM tailordev/pandas

COPY requirements.txt /

RUN pip install --upgrade setuptools pip

RUN pip install -r /requirements.txt

COPY SIE /app
COPY app.py /app
COPY db.py /app
COPY gross.py /app
COPY gross_serializer.py /app
COPY scrapy.cfg /app

WORKDIR /app

CMD ["gunicorn", "-w 4", "main:app"]
