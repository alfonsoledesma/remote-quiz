from db import db

"""
    Basic Model
"""
class Gross(db.Model):
    __tablename__ = "gross"

    index = db.Column(db.Integer, primary_key=True)
    state_name = db.Column(db.String)
    amount = db.Column(db.String)
    month = db.Column(db.String)
    year = db.Column(db.String)

    def __init__(self, state_name, amount, month, year):
        self.state_name = state_name
        self.amount = amount
        self.month = month
        self.year = year
