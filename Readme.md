# SIE Web Scrapping

> The database is also provided through the docker compose file to just type: `docker-compose up -d` and setup both containers.

> In order to use the scrapy spider, you need to configure the splash server. Splash server is already provided in a docker compose file and `docker-compose.yml` file set up quickly and just add the IP address to the server.

> I got two proposals to run the scrapy spider:
    * To run scrapy as a python subprocess and integrate it with flask
    * To run the scrapy spider with a cron job and get another cronjob to reload the data with a curl request  in the API
