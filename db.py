from flask_sqlalchemy import SQLAlchemy
"""Generates connection to the database"""
db = SQLAlchemy()
