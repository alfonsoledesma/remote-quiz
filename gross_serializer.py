from marshmallow import Serializer

"""
Gross data serializer takes marshmallow serializer and returns each field in json format.
You can edit how much parameters do you want to return.
    :param state_name: Name of the federal entity
    :param amoun: Quantity of gross generation
    :param month: Month in which was generated
    :param month: Year in which was generated
    {
        "state_name": "name",
        "amount": "0000.0",
        "month": "Ene",
        "year": "year"
    }
"""
class GrossDataSerializer(Serializer):
    class Meta:
        fields = ('state_name','amount','month','year')

def get_gross_data_serialized(gross_data):
    return GrossDataSerializer(gross_data).data
